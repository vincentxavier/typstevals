//affiche le cartouche nom/prenom
#let nom_prenom() = (
  grid(
     columns: (1fr, 1fr),
     [*Nom : ..........................................*],
     [*Prénom : ..........................................*],
   )
)

// affiche le tableau des compétences
#let tableau_de_compétences(competences: ()) = {
  table(
    columns: (80%, 20%),
    inset: 7pt,
    align: horizon,
    table.header(
      [*Compétences évaluées*],[*Evaluation*]
    ),
    ..competences
  )
}

// transpose un tableau vertical pour qu'il puisse être affiché vericalement
#let transpose(arr, height) = {
  let width = calc.ceil(arr.len() / height)
  let missing = width * height - arr.len()
  // add dummy elements so that the array is "rectangular"
  arr += (none,) * missing
  // transpose the array
  array.zip(..arr.chunks(width)).join()
}

// tableau à trois colonnes
#let three_columns_table_transposed(input_array) = { 
  let transposed_input_array = transpose(input_array, 3)
  let questionIndexArray = range(1, transposed_input_array.len()+1)
  let questionIndexArrayTransposed = transpose(questionIndexArray,3)  
 
  let formatQuestion(inputString, index) = block[
    *Question  #context questionIndexArrayTransposed.at(index) :*
    #inputString     
  ]

  let formattedArray = ()
  let index = 0
  for value in transposed_input_array {
     formattedArray.push(formatQuestion(value, index))
     index = index + 1
  }
  
  table(columns: (1fr, 1fr, 1fr),
    inset: 10pt,
    align: left,
    stroke: none,
    ..formattedArray,
  )
}

// affiche le contenu de l'exercice dans un block pour créer un décalage
#let contenu_exercice(consigne) = {
  block(stroke: none, inset: 1em)[#consigne]
}








