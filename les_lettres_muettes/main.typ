#import "conf.typ": *

#set page(
  margin: (
    top: 1cm,
    bottom: 2cm,
    x: 1cm
  ),
  background: image("rectangle-frame.svg")  
)

\ 

#nom_prenom()

//afficher le tableau de compétences
#let competences = (  
  [- J'ai compris la notion de lettre muette.],
)

#tableau_de_compétences(
  competences: competences
)


#contenu_exercice[
  *Exercice 1* : #underline[*Entoure* la lettre finale muette dans les mots suivants s'il y en a une.]
  
  - chocolat
  - planète
  - rond
  - campagnard
]

#contenu_exercice[
  *Exercice 2* : #underline[*Trouve* la lettre finale.]
  
  - Gran....
  - Peti.....
  - Lour....
  - Len....
  - Froi...
  - Chau.....
]

\ \ \ \ \ \ \

#nom_prenom()

//afficher le tableau de compétences
#let competences = (  
  [- J'ai compris la notion de lettre muette.],
)

#tableau_de_compétences(
  competences: competences
)


#contenu_exercice[
  *Exercice 1* : #underline[*Entoure* la lettre finale muette dans les mots suivants s'il y en a une.]
  
  - chocolat
  - planète
  - rond
  - campagnard
]

#contenu_exercice[
  *Exercice 2* : #underline[*Trouve* la lettre finale.]
  
  - Gran....
  - Peti.....
  - Lour....
  - Len....
  - Froi...
  - Chau.....
]



#contenu_exercice[
  *Exercice 3* : #underline[*Trouve* un mot de la même famille et *trouve* la lettre manquante.]

  Exemple: 
  un bon.... -> ........      un bond -> bondir
  
  - Chan.... → ..............  
  - Déser..... → ...........
  - Froi.... → ..............
  - Gran..... → ..............
  - Len...... -> ............
  - Chocola..... -> ..........
]



\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \


#contenu_exercice[
  *Exercice 3* : #underline[*Trouve* un mot de la même famille et *trouve* la lettre manquante.]

  Exemple: 
  un bon.... -> ........      un bond -> bondir
  
  - Chan.... → ..............  
  - Déser..... → ...........
  - Froi.... → ..............
  - Gran..... → ..............
  - Len...... -> ............
  - Chocola..... -> ..........
]


 







